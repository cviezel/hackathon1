package com.conygre.spring.hackathon1.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.spring.hackathon1.entities.Trade;
import com.conygre.spring.hackathon1.service.TradeService;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TradeControllerUnitTest {

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";
    
    private TradeController controller;

    @BeforeEach
    public void start() {
        ObjectId id = new ObjectId(TEST_ID);
        Trade t = new Trade();
        List<Trade> tradeList = new ArrayList<>();
        tradeList.add(t);

        TradeService service = mock(TradeService.class);
        when(service.getTrades()).thenReturn(tradeList);
        when(service.getTrade(id)).thenReturn(Optional.of(t));

        controller = new TradeController(service);
    }

    @Test
    public void testFindAll() {
        Iterable<Trade> trades = controller.findAll();
        Stream<Trade> stream = StreamSupport.stream(trades.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }

    @Test
    public void testFindTradeById() {
        Optional<Trade> trade = controller.getTradeById(TEST_ID);
        assertThat(trade.isPresent(), equalTo(true));
    }

}