package com.conygre.spring.hackathon1.rest;

import java.util.Optional;

import com.conygre.spring.hackathon1.entities.Trade;
import com.conygre.spring.hackathon1.service.TradeService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/trade")
@CrossOrigin // allows requests from all domains
public class TradeController {
    
    @Autowired
	private TradeService service;
	
	public TradeController(TradeService service) {
		this.service = service;
	}
    
    @RequestMapping(method = RequestMethod.GET)
	public Iterable<Trade> findAll() {
		// logger.info("managed to call a Get request for findAll");
		return service.getTrades();
    }
    
    @RequestMapping(method = RequestMethod.POST)
	public void addTrade(@RequestBody Trade trade) {
		service.addToTrades(trade);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public Optional<Trade> getTradeById(@PathVariable("id") String id) {
		checkValidId(id);
		return service.getTrade(new ObjectId(id));
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void deleteTradeById(@PathVariable("id") String id) {
		checkValidId(id);
		service.removeTrade(new ObjectId(id));
	}

	@RequestMapping(method = RequestMethod.DELETE)
	public void deleteTrade(@RequestBody Trade t) {
		service.removeTrade(t);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/update")
	public void updateTrade(@PathVariable("id") String id, @RequestBody Trade trade) {
		checkValidId(id);
		service.updateTrade(trade, new ObjectId(id));
	}

	private void checkValidId(String id) {
		Optional<Trade> trade = service.getTrade(new ObjectId(id));
		if(!trade.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
}