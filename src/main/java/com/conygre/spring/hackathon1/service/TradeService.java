package com.conygre.spring.hackathon1.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.hackathon1.data.TradeRepository;
import com.conygre.spring.hackathon1.entities.Trade;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeService implements TradeServiceI {
    @Autowired
    private TradeRepository repo;


    @Override
    public void addToTrades(Trade t) {
        repo.insert(t);
    }

    @Override
    public Collection<Trade> getTrades() {
        return repo.findAll();
    }

    @Override
    public Optional<Trade> getTrade(ObjectId id) {
        return repo.findById(id);
    }

    @Override
    public void removeTrade(ObjectId id) {
        repo.deleteById(id);
    }

    @Override
    public void removeTrade(Trade t) {
        repo.delete(t);
    }
    
    @Override
	public void updateTrade(Trade t, ObjectId id) {
        Optional<Trade> oldTradeOpt = getTrade(id);
        Trade oldTrade = oldTradeOpt.get();
		oldTrade.setQuantity(t.getQuantity());
		oldTrade.setRequestedPrice(t.getRequestedPrice());
        oldTrade.setTicker(t.getTicker());
        repo.save(oldTrade);
	}
}