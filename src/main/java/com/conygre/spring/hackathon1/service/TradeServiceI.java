package com.conygre.spring.hackathon1.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.hackathon1.entities.Trade;

import org.bson.types.ObjectId;

public interface TradeServiceI {
    public void addToTrades(Trade disc);
    public Collection<Trade> getTrades();
    public Optional<Trade> getTrade(ObjectId id);
    public void updateTrade(Trade t, ObjectId id);
    public void removeTrade(ObjectId id);
    public void removeTrade(Trade t);
}