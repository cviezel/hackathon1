package com.conygre.spring.hackathon1.entities;

public enum TradeState {
    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED"),
    COMPLETED("COMPLETED");

    private String state;

    private TradeState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    } 
}
