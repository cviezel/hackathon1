package com.conygre.spring.hackathon1.data;

import com.conygre.spring.hackathon1.entities.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TradeRepository extends MongoRepository<Trade, ObjectId> {
    
}